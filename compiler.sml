(* Scheme -> C Compiler *)

datatype SchemeToken = LparenToken
             | RparenToken
             | QuoteToken
             | DotToken
             | VectorToken
             | IntToken of int
             | CharToken of char
             | StringToken of string
             | SymbolToken of string
             | BoolToken of bool;

datatype Sexpr = Void
           | Nil
           | Pair of Sexpr * Sexpr
           | Vector of Sexpr list
           | Symbol of string
           | String of string
           | Number of int
           | Bool of bool
           | Char of char;

datatype Expr = Const of Sexpr
              | Var of string
              | VarFree of string              (* free variable *)
              | VarParam of string * int       (* parameter variable *)
              | VarBound of string * int * int (* bound variable *)
              | If of Expr * Expr * Expr
              | Abs of (string list) * Expr
              | AbsOpt of (string list) * string * Expr
              | AbsVar of string * Expr
              | App of Expr * (Expr list)
              | AppTP of Expr * (Expr list)   (* in tail position *)
              | Seq of Expr list
              | Or of Expr list
              | Set of Expr * Expr
              | Def of Expr * Expr;

use "./src/sml/utils.sml";      (* Utility functions *)
use "./src/sml/scanner.sml";    (* Scanner *)
use "./src/sml/reader.sml";     (* Reader *)
use "./src/sml/tag-parser.sml"; (* Tag Parser *)
use "./src/sml/sa.sml";         (* Semantic Analysis *)
use "./src/sml/cg.sml";         (* Code Generation *)

signature CODE_GEN =
sig
    val cg : Expr -> string;
    val compileSchemeFile : string * string -> unit;
end;

fun stringToFile (filename : string, str : string) =
    let
        val f = TextIO.openOut filename
    in
      ( TextIO.output(f, str);
        TextIO.closeOut f )
    end;

fun fileToString (filename : string) =
    let
        val f = TextIO.openIn filename
        fun loop s =
           (case TextIO.input1 f of
                NONE => s
              | SOME c => loop (c :: s))
        val result = String.implode (rev (loop []))
    in
       (TextIO.closeIn f;
        result)
    end;

structure CodeGen : CODE_GEN =
struct

    fun cg (e : Expr) : string =
    (Program.reset ();
        (Program.gen e 0);
        Program.emit (8,1024*10))

    fun compile str =
        (Program.reset ()
        ;map (fn expr => (Program.gen (SemanticAnalysis.semanticAnalysis expr) 0))
                (TagParser.stringToPEs str)
        ;Program.emit (8,1024*10)
        );

    fun compileSchemeFile (infile:string, outfile:string) : unit =
        stringToFile (
            outfile,
            (compile ((*(fileToString "./src/scm/support-code.scm")^*)
                      "((lambda() "^(fileToString infile)^"))" )));

end; (* of struct CodeGen *)


