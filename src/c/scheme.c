/* scheme.c
 * The run-time support needed for the Scheme->C compiler.
 * Programmer: Mayer Goldberg, 2000
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "assertions.h" /* check this out: great for debugging */
#include "scheme.h"

#define STRING_ALLOC(s, n) do { \
          s = (char *)autoMalloc((n)*sizeof(char)); \
        } while (0)

SchemeObject *makeSchemeObject()
{
  SchemeObject *sob;

  sob = (SchemeObject *)autoMalloc(sizeof(SchemeObject));
  SOB_DATA(sob) = NULL;

  return sob;
}

SchemeObject *makeSchemeVoid()
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_VOID;
  SOB_DATA(sob) = NULL;

  return sob;
}

SchemeObject *makeSchemeInt(int n)
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_INT;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeIntData)));
  SOB_INT_VALUE(sob) = n;

  return sob;
}

SchemeObject *makeSchemeChar(char c)
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_CHAR;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeCharData)));
  SOB_CHAR_VALUE(sob) = c;

  return sob;
}

SchemeObject *makeSchemeBool(int b)
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_BOOL;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeBoolData)));

  SOB_BOOL_VALUE(sob) = b;

  return sob;
}

SchemeObject *makeSchemeString(int n, char c)
{
  SchemeObject *sob;
  int i;

  ASSERT_ALWAYS(n >= 0,"");

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_STRING;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeStringData)));

  SOB_STRING_LENGTH(sob) = n;
  SOB_STRING_VALUE(sob) = (char *)autoMalloc(n + 1);

  for (i = 0; i < n; ++i) {
    SOB_STRING_SET(sob, i, c);
  }

  SOB_STRING_SET(sob, n, '\0');

  return sob;
}

SchemeObject *makeSchemeStringFromCString(char *s)
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_STRING;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeStringData)));

  SOB_STRING_LENGTH(sob) = strlen(s);
  SOB_STRING_VALUE(sob) = (char *)autoMalloc(1 + SOB_STRING_LENGTH(sob));

  strcpy(SOB_STRING_VALUE(sob), s);

  return sob;
}

SchemeObject *makeSchemeNil()
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_NIL;

  return sob;
}

SchemeObject *makeSchemePair(SchemeObject *car, SchemeObject *cdr)
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_PAIR;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemePairData)));

  SOB_PAIR_CAR(sob) = car;
  SOB_PAIR_CDR(sob) = cdr;

  return sob;
}

SchemeObject *makeSchemeVectorInit(int n, SchemeObject *initSob)
{
  SchemeObject *sob;
  int i;

  ASSERT_ALWAYS(n >= 0,"");

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_VECTOR;
  SOB_DATA(sob) = (SchemeObjectData *)autoMalloc(sizeof(SchemeVectorData));

  SOB_VECTOR_LENGTH(sob) = n;

  SOB_VECTOR_VALUE(sob) = 
    (SchemeObject **)autoMalloc(n * sizeof(SchemeObject *));

  for (i = 0; i < n; ++i) {
    SOB_VECTOR_SET(sob, i, initSob);
  }

  return sob;
}

extern SymbolNode *topLevel;

SchemeObject *makeSchemeSymbol(char *s)
{
  int len;
  SchemeObject *sob;

  len = strlen(s);
  ASSERT_ALWAYS(len > 0,"");

  sob = makeSchemeObject();
  SOB_TYPE(sob) = SCHEME_SYMBOL;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeSymbolData)));

  SOB_SYMBOL_ENTRY(sob) = getSymbol(s, topLevel);

  return sob;
}

SchemeObject *makeSchemeClosure(void* env,void* code)
{
  SchemeObject *sob;

  sob = makeSchemeObject();
  ASSERT_ALWAYS( sob!=0,"" );
  SOB_TYPE(sob) = SCHEME_CLOSURE;
  SOB_DATA(sob) = SOD(autoMalloc(sizeof(SchemeClosureData)));
  ASSERT_ALWAYS( SOB_DATA(sob)!=0,"" );

  SOB_CLOSURE_ENV(sob) = env;
  SOB_CLOSURE_CODE(sob) = code;

  return sob;
}

char *sobVoidToString(SchemeObject *sob);
char *sobIntToString(SchemeObject *sob);
char *sobCharToString(SchemeObject *sob);
char *sobBoolToString(SchemeObject *sob);
char *sobStringToString(SchemeObject *sob);
char *sobNilToString(SchemeObject *sob);
char *sobPairToString(SchemeObject *sob);
char *sobVectorToString(SchemeObject *sob);
char *sobSymbolToString(SchemeObject *sob);
char *sobClosureToString(SchemeObject *sob);

char *sobToString(SchemeObject *sob)
{
  SchemeType sobType;

  sobType = SOB_TYPE(sob);
  switch (sobType) {
  case SCHEME_VOID:           return sobVoidToString(sob);
  case SCHEME_INT:            return sobIntToString(sob);
  case SCHEME_CHAR:           return sobCharToString(sob);
  case SCHEME_BOOL:           return sobBoolToString(sob);
  case SCHEME_STRING:         return sobStringToString(sob);
  case SCHEME_NIL:            return sobNilToString(sob);
  case SCHEME_PAIR:           return sobPairToString(sob);
  case SCHEME_VECTOR:         return sobVectorToString(sob);
  case SCHEME_SYMBOL:         return sobSymbolToString(sob);
  case SCHEME_CLOSURE:        return sobClosureToString(sob);
  default:
    ASSERT_ALWAYS((sobType >= SCHEME_TYPE_FIRST) && 
                  (sobType < SCHEME_TYPE_END),"");

    fprintf(stderr,
            "Non exhaustive switch in file \"%s\", line %d\n",
                    __FILE__, __LINE__);
            exit(-1);
  }
}

#define VOID_PRINT_STRING "#<void object>"

char *sobVoidToString(SchemeObject *sob)
{
  char *res;

  STRING_ALLOC(res, 1 + sizeof(VOID_PRINT_STRING));
  sprintf(res, "%s", VOID_PRINT_STRING);

  return res;
}

#define MAX_INTEGER_LENGTH 32

char *sobIntToString(SchemeObject *sob)
{
  char buf[MAX_INTEGER_LENGTH], *res;

  sprintf(buf, "%d", SOB_INT_VALUE(sob));

  STRING_ALLOC(res, 1 + strlen(buf));
  strcpy(res, buf);

  return res;
}

#define MAX_CHAR_LENGTH 32

char *sobCharToString(SchemeObject *sob)
{
  unsigned char c;
  char buf[MAX_CHAR_LENGTH], *res;

  c = SOB_CHAR_VALUE(sob);

  if (c == ' ') {
    sprintf(buf, "#\\space");
  }
  else if (c == '\n') {
    sprintf(buf, "#\\newline");
  }
  else if (c == '\r') {
    sprintf(buf, "#\\return");
  }
  else if (c == '\f') {
    sprintf(buf, "#\\page");
  }
  else if (c == '\0') {
    sprintf(buf, "#\\nul");
  }
  else if (c < ' ') {
    unsigned int o1, o2, o3;

    o3 = c % 8; c = c >> 3;
    o2 = c % 8; c = c >> 3;
    o1 = c % 8; /* not needed; just for good luck :) */
    sprintf(buf, "#\\%u%u%u", o1, o2, o3);
  }
  else {
    sprintf(buf, "#\\%c", c);
  }

  STRING_ALLOC(res, 1 + strlen(buf));
  strcpy(res, buf);

  return res;
}

#define MAX_BOOLEAN_LENGTH 32

char *sobBoolToString(SchemeObject *sob)
{
  char buf[MAX_BOOLEAN_LENGTH], *res;
  int b;

  b = SOB_BOOL_VALUE(sob);
  switch (b) {
  case 0: sprintf(buf, "#f"); break;
  case 1: sprintf(buf, "#t"); break;
  default:
      ASSERT_ALWAYS((b != 0) && (b != 1),"");
  }

  STRING_ALLOC(res, 1 + strlen(buf));
  strcpy(res, buf);

  return res;
}

char *sobStringToString(SchemeObject *sob)
{
  char *src, *dst, *res;
  int srcLen, i, j;

  srcLen = SOB_STRING_LENGTH(sob);
  src = SOB_STRING_VALUE(sob);
  dst = (char *)autoMalloc(1 + (srcLen << 1)); /* max possible dst length */

  for (i = 0, j = 0; i < srcLen; ++i) {
    switch (src[i]) {
    case '\\':
      dst[j++] = '\\'; dst[j++] = '\\'; break;
    case '"':
      dst[j++] = '\\'; dst[j++] = '"'; break;
    default:
      dst[j++] = src[i];
    }
  }

  dst[j] = '\0';

  STRING_ALLOC(res, 3 + strlen(dst));
  strcpy(res, "\"");
  strcat(res, dst);
  strcat(res, "\"");

  return res;
}

#define MAX_NIL_LENGTH 32

char *sobNilToString(SchemeObject *sob)
{
  char buf[MAX_NIL_LENGTH], *res;

  sprintf(buf, "()");

  STRING_ALLOC(res, 1 + strlen(buf));
  strcpy(res, buf);

  return res;
}

char *sobPairToString(SchemeObject *sob)
{
  char *tmp1, *tmp2, *res;
  SchemeObject *cdr;

  tmp1 = sobToString(SOB_PAIR_CAR(sob));
  cdr = SOB_PAIR_CDR(sob);
  if (IS_SOB_NIL(cdr)) {
    STRING_ALLOC(res, 3 + strlen(tmp1));
    sprintf(res, "(%s)", tmp1); autoFree(tmp1);
  }
  else if (IS_SOB_PAIR(cdr)) {
    tmp2 = sobToString(cdr);
    STRING_ALLOC(res, 3 + strlen(tmp1) + strlen(tmp2));
    sprintf(res, "(%s %s", tmp1, tmp2 + 1);
  }
  else {
    tmp2 = sobToString(cdr);
    STRING_ALLOC(res, 6 + strlen(tmp1) + strlen(tmp2));
    sprintf(res, "(%s . %s)", tmp1, tmp2);
    autoFree(tmp1); autoFree(tmp2);
  }

  return res;
}

#define MAX_VECTOR_TEMP_LENGTH 32

char *sobVectorToString(SchemeObject *sob)
{
  char *res, tmp[MAX_VECTOR_TEMP_LENGTH];
  int len, i;

  len = SOB_VECTOR_LENGTH(sob);
  if (len == 0) {
    STRING_ALLOC(res, 5);
    strcpy(res, "#0()");

    return res;
  }
  else {
    char **eltStr;
    int resSize;

    eltStr = (char **)autoMalloc(len * sizeof(char *));

    for (i = 0, resSize = 2 + len; i < len; ++i) {
      eltStr[i] = sobToString(SOB_VECTOR_REF(sob, i));
      resSize += strlen(eltStr[i]);
    }

    sprintf(tmp, "#%d(", len);
    resSize += strlen(tmp);

    STRING_ALLOC(res, resSize);
    strcpy(res, tmp);
    strcat(res, eltStr[0]); autoFree(eltStr[0]);

    for (i = 1; i < len; ++i) {
      strcat(res, " ");
      strcat(res, eltStr[i]); autoFree(eltStr[i]);
    }

    autoFree(eltStr);

    strcat(res, ")");

    return res;
  }
}

#define SYMBOL_CHARS "abcdefghijklmnopqrstuvwxyz01234567890!@$%^&*_-+=/?:<>,."

char *sobSymbolToString(SchemeObject *sob)
{
  char *res, *tmp; /*, *buf; */
  int len, i, isSpecial;


  tmp = SOB_SYMBOL_NAME(sob);
  len = strlen(tmp);

  for (i = 0, isSpecial = 0; i < len; ++i) {
    if (strchr(SYMBOL_CHARS, tmp[i]) == NULL) {
      isSpecial = 1;
      break;
    }
  }

  if (isSpecial) {
    STRING_ALLOC(res, 3 + len);
    sprintf(res, "|%s|", tmp);
  } 
  else {
    STRING_ALLOC(res, 1 + len);
    sprintf(res, "%s", tmp);
  }

  return res;
}

#define CLOSURE_STRING "#<compiled closure>"

char *sobClosureToString(SchemeObject *sob)
{
  char *res;

  STRING_ALLOC(res, 1 + sizeof(CLOSURE_STRING));
  strcpy(res, CLOSURE_STRING);

  return res;
}

/* support for the top level; this is not a hash table -- see header
 * file for details.  
 */

SymbolEntry *probeSymbolDefined(char *name, SymbolNode *t)
{
  int compare;

  if (t == NULL) { return NULL; }

  compare = strcmp(name, SYM_NAME(t));
  if (compare == 0) {
    return SYM_ENTRY(t);
  }
  else if (compare < 0) {
    return probeSymbolDefined(name, SYM_LEFT(t));
  } 
  else {
    return probeSymbolDefined(name, SYM_RIGHT(t));
  }
}

SymbolNode *newSymbolNode(char *name);

SymbolEntry *getSymbol(char *name, SymbolNode *t)
{
  int compare;
  SymbolNode *child;

  if (t == NULL) {
    if (t == topLevel) {
      topLevel = newSymbolNode(name);

      return SYM_ENTRY(topLevel);
    }
    else {
        ASSERT_ALWAYS(t != NULL,"");
    }
  }

  compare = strcmp(name, SYM_NAME(t));

  if (compare == 0) {
    return SYM_ENTRY(t);
  }
  else if (compare < 0) {
    child = SYM_LEFT(t);
    if (child == NULL) {
      child = newSymbolNode(name);
      SYM_LEFT(t) = child;

      return SYM_ENTRY(child);
    }
    else {
      return getSymbol(name, child);
    }
  }
  else {
    child = SYM_RIGHT(t);
    if (child == NULL) {
      child = newSymbolNode(name);
      SYM_RIGHT(t) = child;

      return SYM_ENTRY(child);
    }
    else {
      return getSymbol(name, child);
    }
  }
}

SymbolNode *newSymbolNode(char *name)
{
  SymbolNode *child;

  child = (SymbolNode *)autoMalloc(sizeof(SymbolNode));
  SYM_ENTRY(child) = (SymbolEntry *)autoMalloc(sizeof(SymbolEntry));
  SYM_NAME(child) = (char *)autoMalloc(1 + strlen(name));
  strcpy(SYM_NAME(child), name);
  SYM_HAS_VALUE(child) = 0;
  SYM_VALUE(child) = NULL;
  SYM_LEFT(child) = SYM_RIGHT(child) = NULL;

  return child;
}

/* The procedures autoMalloc and autoFree are interface procedures:
 * They are meant to be used instead of malloc and free, and are to be
 * re-defined when the garbage collector is added to the system. Hint:
 * autoFree will be defined to do nothing. :)
 */
void *autoMalloc(int n)
{
  void *tmp;

  if (n == 0) {
    return NULL;
  }
  else {
    tmp = (void *)malloc(n);
    ASSERT_ALWAYS(tmp != NULL,"");

    return tmp;
  }
}

void autoFree(void *p)
{
  if (p != NULL) {
    free(p);
  }
}


