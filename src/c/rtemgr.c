#include "rtemgr.h"
#include "scheme.h"
#include "arch.h"
#include "assertions.h"
#include "strings.h"

extern SchemeObject sc_nil;

void printActFrm(int p) {
    unsigned int fp,ret,env,n;
    unsigned int i;

    fp = stack[p-1];
    ret = stack[p-2];
    env = stack[p-3];
    n = stack[p-4];

    fprintf( stderr, "[%d] fp=0x%x\n", p-1, fp );  fflush(stderr);
    fprintf( stderr, "[%d] ret=0x%x\n", p-2, ret );  fflush(stderr);
    fprintf( stderr, "[%d] env=0x%x\n", p-3, env );  fflush(stderr);
    fprintf( stderr, "[%d] n=%d\n", p-4, n );  fflush(stderr);
    for (i=p-5; i>=p-5-n+1; i--) {
        fprintf( stderr, "[%d] a[%d]=%s\n",i, -i+p-5, sobToString((SchemeObject*)(stack[i])) );
        fflush(stderr);
    }
}

/* Copies the enviroment vector currenv to a new vector of size
   (currenv_size+1) starting from element #1.
   Copies the arguments from the stack to element #0 of the new vector.
   Returns the new env. vector.

   @param currenv: points to the current enviroment vector
   @param currenv_size: size of the current enviroment vector
*/
int** extendEnviroment(int** currenv, int currenv_size)
{
    int** newenv;   /* newenv: points to the new envoriment vector */
    int ndx;        /* ndx: loop index */

    ASSERT_ALWAYS(currenv_size>=0,"");

    /* allocate new enviroment vector */
    newenv = autoMalloc( sizeof(int) * (currenv_size + 1) );

    /* copy current enviroment to new enviroment */
    ndx = currenv_size;
Lfor1:
    if (ndx<=0) goto LendFor1;
    newenv[ndx] = currenv[ndx-1];
    --ndx;
    goto Lfor1;
LendFor1:
    /* copy arguments from stack to new enviroment vector (extend env.) */
    if (currenv_size<=0) goto LendExtend1; /* no activation frame at all */
    if (ST_ARG_COUNT()<=0) goto LendExtend1; /* no args in act. frame */
    newenv[0] = autoMalloc( sizeof(int) * ST_ARG_COUNT() );
    ndx = 0;
Lfor2:
    if (ndx>=ST_ARG_COUNT()) goto LendFor2;
    newenv[0][ndx] = ST_ARG(ndx);
    ++ndx;
    goto Lfor2;
LendFor2:
LendExtend1:

    return newenv;
}

/* Prepares the stack for a tail-call.
   Overrides the current activation frame with the new one,
   fixes sp,fp
*/
void shiftActFrmDown()
{
    int low;       /* lowest address of the new activation frame (included) */
    int high;      /* highest address of the new activation frame (included) */
    int dest_low;  /* lowest address where to put the new activation frame */

    int old_fp = ST_OLDFP(); /* save it as its about to be overwritten */

    /* Shift activation frame down the stack */
    low = fp;
    high = sp-1;
    dest_low = ST_FRMEND();

    while (low <= high) {
        stack[dest_low] = stack[low];
        ++dest_low;
        ++low;
    }

    /* Fix sp,fp */
    fp = old_fp;
    sp = dest_low;
}

/* Shifts the elements in the stack upwards.

   @param pos: index of the first element to shift
   @param amount: the number of elements to shift up each element by

   Shifts up each element between `pos` and sp (inclusive) by `amount`.
*/
void shiftStackUp(int pos, unsigned int amount)
{
    int i;

    /* shift elements upwards */
    for (i=sp-1; i>=pos; --i) {
        stack[i+amount] = stack[i];
    }

    /* fix sp,fp */
    sp = sp + amount;
    fp = fp + amount;
}

/* Shifts the elements in the stack downwards.

   @param pos: index of the first element to shift
   @param amount: the number of elements to shift down each element by

   Shifts down each element between `pos` and sp (inclusive) by `amount`.
*/
void shiftStackDown(int pos, unsigned int amount)
{
    int i;

    /* shift elements downwards */
    for (i=pos; i<=sp-1; ++i) {
        stack[i-amount] = stack[i];
    }

    /* fix sp,fp */
    sp = sp - amount;
    fp = fp - amount;
}

/* Prepares the stack for an application of lambda-optional.

   @param formalParams: the number of formal parameters the lambda
          expects, including the optional one.

   Moves the optional parameters to a list in the heap.
*/
void prepareStackForAbsOpt(int formalParams)
{
    int actualParams = ST_ARG_COUNT();
    int optionalsUsed = actualParams-(formalParams-1);

    ASSERT_ALWAYS( optionalsUsed>=0, "" );

    if (optionalsUsed==0) {
        /* if the optional parameter was not used, initialize it to an
           empty list: */

        /* make room for the optional param */
        shiftStackUp( ST_FRMEND(), 1 );

        /* fix actual arguments number (also affects ST_FRMEND)*/
        ST_ARG_COUNT() = formalParams;

        /* set it to () */
        stack[ ST_FRMEND() ] = (int)&sc_nil;
    }
    else {
        /* if the optional parameters were used, move them to a list
           on the heap: */

        SchemeObject* opt;  /* the list of optional params */
        int lastOptional;   /* last opt. param. (lowest index) */
        int firstOptional;  /* first opt. param. (highest index) */
        int i;

        /* copy optional params to a list on the heap */
        opt = &sc_nil;
        lastOptional = ST_FRMEND();
        firstOptional = lastOptional + (optionalsUsed-1);

        for (i=lastOptional; i<=firstOptional; ++i) {
            opt = makeSchemePair( (SchemeObject*)(stack[i]), opt );
        }

        /* override all but one optional */
        shiftStackDown( firstOptional, optionalsUsed-1 );

        /* fix actual arguments number (also affects ST_FRMEND) */
        ST_ARG_COUNT() = formalParams;

        /* the last parameter is a pointer to the new list */
        stack[ ST_FRMEND() ] = (int)opt;
    }

}

/* Reverses a Scheme list (in place)

   @param list: the list to reverse
   @return: the reversed list
*/
SchemeObject* reverseSchemeList( SchemeObject* list )
{
    SchemeObject* prev;
    SchemeObject* curr;
    SchemeObject* next;

    prev = &sc_nil;
    curr = list;

    while ( curr!=&sc_nil ) {
        ASSERT_ALWAYS( IS_SOB_PAIR(curr), MSG_ERR_NOTLIST );
        next = SOB_PAIR_CDR(curr);
        SOB_PAIR_CDR(curr) = prev;
        prev = curr;
        curr = next;
    }

    return prev;
}

/* Pushes the argument list (and the number of arguments in the list
   onto the stack

   @param list: the arguments list

   The arguments are pushed backwards.
*/
void pushArgsList(SchemeObject* list)
{
    /* strategy:
        1. reverse list
        2. push each argument
        3. push the number of arguments
        4. reverse again
    */

    SchemeObject* pair;
    int args_count;

    list = reverseSchemeList( list );

    args_count = 0;
    pair = list;
    while (pair != &sc_nil) {
        push( (int)SOB_PAIR_CAR(pair) );
        pair = SOB_PAIR_CDR(pair);
        args_count++;
    }
    push( args_count );

    list = reverseSchemeList( list );
}
