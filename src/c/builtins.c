/* <Built-in procedures> */

/* Creates a closure and adds it to the top-level
   @param name (string) the name of the built-in procedure
   @param code (void*) the address of the built-in procedure (label address)
*/
#define CREATE_BUILTIN_CLOS(name,code) \
    r_res = (int)getSymbol((name),topLevel); /* create a hash bucket */ \
    ((SymbolEntry*)r_res)->sob = makeSchemeClosure(NULL,(code)); \
    ASSERT_ALWAYS( ((SymbolEntry*)r_res)->sob!=NULL,"" ); \
    ((SymbolEntry*)r_res)->isDefined = 1;

/* The stack when in a built-in precedure (there is no fp):
  sp -> |      |
        | ret  |  - return address
        | env  |  - points to the enviroment vector
        | n    |  - number of arguments
        | A0   |  - argument 0
        | A1   |  - argument 1
        | ...  |  - ...
        | An-1 |  - argument n-1

   Macros for built-in procedures:
*/
#define BI_ST_ARG(n) (stack[sp-4-(n)])
#define BI_ST_ARG_COUNT() (stack[sp-3])
#define BI_ST_RET() (stack[sp-1])
#define BI_RETURN() goto *pop()

    goto Lstart; /* skip all definitions */

Lcar:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("car",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_PAIR(r[0]), MSG_ERR_NOTPAIR );
    r_res = (int)SOB_PAIR_CAR(r[0]);
    BI_RETURN();

Lcdr:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("cdr",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_PAIR(r[0]), MSG_ERR_NOTPAIR );
    r_res = (int)SOB_PAIR_CDR(r[0]);
    BI_RETURN();

Lsymbol:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("symbol?",1) );
    r[0] = BI_ST_ARG(0);
    if ( IS_SOB_SYMBOL(r[0]) ) goto Lsymbol_true;
    r_res = (int)&sc_false;
    BI_RETURN();
Lsymbol_true:
    r_res = (int)&sc_true;
    BI_RETURN();

Lset_car:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("set-car!",2) );
    r[0] = BI_ST_ARG(0);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS( IS_SOB_PAIR(r[0]), MSG_ERR_NOTPAIR );
    SOB_PAIR_CAR(r[0]) = (SchemeObject*)r[1];
    r_res = (int)&sc_void;
    BI_RETURN();

Lset_cdr:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("set-cdr!",2) );
    r[0] = BI_ST_ARG(0);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS( IS_SOB_PAIR(r[0]), MSG_ERR_NOTPAIR );
    SOB_PAIR_CDR(r[0]) = (SchemeObject*)r[1];
    r_res = (int)&sc_void;
    BI_RETURN();

Lmake_vector:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()>=1,MSG_ERR_ARGCOUNT("make-vector",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE(r[0]);
    r_res = (int)makeSchemeVectorInit(r[0],&sc_void);
    /* initialize vector elements */
    r[2] = (int)&sc_void;
    r[1] = 0;
    for (r[1]=0; r[1]<r[0]; ++r[1]) {
      if (r[1]+1<BI_ST_ARG_COUNT()) r[2]=BI_ST_ARG(r[1]+1);
      SOB_VECTOR_SET(r_res,r[1],(void*)r[2]);
    }
    BI_RETURN();

Lvector_set:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==3,MSG_ERR_ARGCOUNT("vector-set!",3) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_VECTOR((SchemeObject*)r[0]), "");
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    r[2] = BI_ST_ARG(2);
    SOB_VECTOR_SET((SchemeObject*)r[0],r[1],(SchemeObject*)r[2]);
    r_res = (int)&sc_void;
    BI_RETURN();

Lvector_ref:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("vector-ref",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_VECTOR((SchemeObject*)r[0]), "");
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    r_res = (int)SOB_VECTOR_REF((SchemeObject*)r[0],r[1]);
    BI_RETURN();

Lvector_length:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("vector-length",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_VECTOR(r[0]), "" );
    r_res = SOB_VECTOR_LENGTH(r[0]);
    r_res = (int)makeSchemeInt(r_res);
    BI_RETURN();

LbinaryEQ:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("binary=?",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE((SchemeObject*)r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    if (r[0] != r[1]) goto LbinaryEQ_false;
    r_res = (int)&sc_true;
    BI_RETURN();
LbinaryEQ_false:
    r_res = (int)&sc_false;
    BI_RETURN();

LbinaryADD:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("binary-add",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE((SchemeObject*)r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    r_res = r[0] + r[1];
    r_res = (int)makeSchemeInt( r_res );
    BI_RETURN();

LbinarySUB:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("binary-sub",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE((SchemeObject*)r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    r_res = r[0] - r[1];
    r_res = (int)makeSchemeInt( r_res );
    BI_RETURN();

LbinaryMUL:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("binary-mul",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE((SchemeObject*)r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    r_res = (int)r[0] * (int)r[1]; /* multiply as signed integers */
    r_res = (int)makeSchemeInt( r_res );
    BI_RETURN();

LbinaryDIV:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("binary-div",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE((SchemeObject*)r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    ASSERT_ALWAYS( (int)r[1]!=0, MSG_ERR_DIVZERO );
    r_res = (int)r[0] / (int)r[1]; /* divide as signed integers */
    r_res = (int)makeSchemeInt( r_res );
    BI_RETURN();

LbinaryLT:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("binary<?",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE((SchemeObject*)r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[1]), "");
    r[1] = SOB_INT_VALUE((SchemeObject*)r[1]);
    r_res = ((int)r[0] < (int)r[1]); /* compare as signed integers */
    if ( r_res ) goto LbinaryLT_true;
    r_res = (int)&sc_false;
    BI_RETURN();
LbinaryLT_true:
    r_res = (int)&sc_true;
    BI_RETURN();

Lbox:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("box",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (int)makeSchemeVectorInit( 1,(SchemeObject*)(r[0]) );
    BI_RETURN();

Lnull:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("null?",1) );
    r[0] = BI_ST_ARG(0);
    if ( IS_SOB_NIL(r[0]) ) goto Lnull_true;
    r_res = (int)&sc_false;
    BI_RETURN();
Lnull_true:
    r_res = (int)&sc_true;
    BI_RETURN();

Lchar_to_integer:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("char->integer",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_CHAR(r[0]), "" );
    r[0] = (unsigned char)SOB_CHAR_VALUE(r[0]);
    r_res = (int)makeSchemeInt( (int)r[0] );
    BI_RETURN();

Linteger_to_char:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("integer->char",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_INT(r[0]), "" );
    r[0] = SOB_INT_VALUE(r[0]);
    r_res = (int)makeSchemeChar( (char)r[0] );
    BI_RETURN();

Lapply:
    push( fp );
    fp = sp;
    ASSERT_ALWAYS( ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("apply",2) );
    r[0] = ST_ARG(0); /* r[0] is now the closure */
    ASSERT_ALWAYS( IS_SOB_CLOSURE(r[0]), MSG_ERR_APPNONPROC );
    r[1] = ST_ARG(1); /* r[1] is now the list of arguments */
    ASSERT_ALWAYS( (r[1]==(int)&sc_nil) | (IS_SOB_PAIR(r[1])), MSG_ERR_NOTLIST );
    /* push arguments (backwards) and number of arguments */
    pushArgsList( (SchemeObject*)r[1] );
    /* push env. of closure */
    push( (int)SOB_CLOSURE_ENV(r[0]) );
    /* push current return address (it's a tail call) */
    push( (int)ST_RET() );
    /* override current frame (it's a tail call) */
    shiftActFrmDown();
    /* branch */
    goto *SOB_CLOSURE_CODE(r[0]);

Lboolean:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("boolean?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_BOOL(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lchar:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("char?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_CHAR(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Linteger:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("integer?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_INT(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lpair:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("pair?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_PAIR(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lprocedure:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("procedure?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_CLOSURE(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lstring:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("string?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_STRING(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lvector:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("vector?",1) );
    r[0] = BI_ST_ARG(0);
    r_res = (IS_SOB_VECTOR(r[0])) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lzero:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("zero?",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_INT(r[0]), "" );
    r[0] = SOB_INT_VALUE(r[0]);
    r_res = (r[0]==0) ? (int)&sc_true : (int)&sc_false;
    BI_RETURN();

Lcons:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("cons",2) );
    r[0] = BI_ST_ARG(0);
    r[1] = BI_ST_ARG(1);
    r_res = (int)makeSchemePair( (SchemeObject*)r[0], (SchemeObject*)r[1] );
    BI_RETURN();

Leq:
    /* The eq? procedure compare the
       VALUES of booleans,
                 chars,
                 integers,
                 symbols,
       and the ADDRESSES of pairs,
                            strings, and 
                            vectors.
    */
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("eq?",2) );
    r[0] = BI_ST_ARG(0);
    r[1] = BI_ST_ARG(1);
    if ( IS_SOB_BOOL(r[0]) ) goto Leq_boolval;
    if ( IS_SOB_CHAR(r[0]) ) goto Leq_charval;
    if ( IS_SOB_INT(r[0]) ) goto Leq_intval;
    if ( IS_SOB_SYMBOL(r[0]) ) goto Leq_symval;
    if ( r[0]==r[1] ) goto Leq_true;
    goto Leq_false;
Leq_boolval:
    if ( !IS_SOB_BOOL(r[1]) ) goto Leq_false;
    if ( SOB_BOOL_VALUE(r[0])==SOB_BOOL_VALUE(r[1]) ) goto Leq_true;
    goto Leq_false;
Leq_charval:
    if ( !IS_SOB_CHAR(r[1]) ) goto Leq_false;
    if ( SOB_CHAR_VALUE(r[0])==SOB_CHAR_VALUE(r[1]) ) goto Leq_true;
    goto Leq_false;
Leq_intval:
    if ( !IS_SOB_INT(r[1]) ) goto Leq_false;
    if ( SOB_INT_VALUE(r[0])==SOB_INT_VALUE(r[1]) ) goto Leq_true;
    goto Leq_false;
Leq_symval:
    if ( !IS_SOB_SYMBOL(r[1]) ) goto Leq_false;
    if ( SOB_SYMBOL_ENTRY(r[0])==SOB_SYMBOL_ENTRY(r[1]) ) goto Leq_true;
    goto Leq_false;
Leq_true:
    r_res = (int)&sc_true;
    BI_RETURN();
Leq_false:
    r_res = (int)&sc_false;
    BI_RETURN();

Lmake_string:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()>=1,MSG_ERR_ARGCOUNT("make-string",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS(IS_SOB_INT((SchemeObject*)r[0]), "");
    r[0] = SOB_INT_VALUE(r[0]);
    r[1] = 0;
    if (BI_ST_ARG_COUNT()==1) goto Lmake_string_make;
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS(IS_SOB_CHAR((SchemeObject*)r[1]), "");
    r[1] = (int)SOB_CHAR_VALUE(r[1]);
Lmake_string_make:
    r_res = (int)makeSchemeString(r[0],(char)r[1]);
    BI_RETURN();    

Lremainder:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("remainder",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_INT(r[0]), "" );
    r[0] = SOB_INT_VALUE(r[0]);
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS( IS_SOB_INT(r[1]), "" );
    r[1] = SOB_INT_VALUE(r[1]);
    ASSERT_ALWAYS( (int)r[1]!=0, MSG_ERR_DIVZERO );
    r_res = (int)r[0] % (int)r[1]; /* divide as signed integers */
    r_res = (int)makeSchemeInt( r_res );
    BI_RETURN();

Lstring_to_symbol:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("string->symbol",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_STRING(r[0]), "" );
    r[0] = (int)SOB_STRING_VALUE(r[0]);
    r_res = (int)makeSchemeSymbol((char*)r[0]);
    BI_RETURN();

Lsymbol_to_string:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("symbol->string",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_SYMBOL(r[0]), "" );
    r[0] = (int)SOB_SYMBOL_NAME(r[0]);
    r_res = (int)makeSchemeStringFromCString( (char*)r[0] );
    BI_RETURN();

Lstring_length:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==1,MSG_ERR_ARGCOUNT("string-length",1) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_STRING(r[0]), "" );
    r_res = SOB_STRING_LENGTH(r[0]);
    r_res = (int)makeSchemeInt(r_res);
    BI_RETURN();

Lstring_ref:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==2,MSG_ERR_ARGCOUNT("string-ref",2) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_STRING(r[0]), "" );
    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS( IS_SOB_INT(r[1]), "" );
    r[1] = SOB_INT_VALUE( r[1] );
    ASSERT_ALWAYS( (r[1]>=0) & (r[1]<SOB_STRING_LENGTH(r[0])), MSG_ERR_NDXOUTOFBOUNDS );
    r_res = SOB_STRING_REF(r[0],r[1]);
    r_res = (int)makeSchemeChar(r_res);
    BI_RETURN();

Lstring_set:
    ASSERT_ALWAYS( BI_ST_ARG_COUNT()==3,MSG_ERR_ARGCOUNT("string-set!",3) );
    r[0] = BI_ST_ARG(0);
    ASSERT_ALWAYS( IS_SOB_STRING(r[0]), "" );

    r[1] = BI_ST_ARG(1);
    ASSERT_ALWAYS( IS_SOB_INT(r[1]), "" );
    r[1] = SOB_INT_VALUE( r[1] );
    ASSERT_ALWAYS( (r[1]>=0) & (r[1]<SOB_STRING_LENGTH(r[0])), MSG_ERR_NDXOUTOFBOUNDS );

    r[2] = BI_ST_ARG(2);
    ASSERT_ALWAYS( IS_SOB_CHAR(r[2]), "" );
    r[2] = SOB_CHAR_VALUE( r[2] );
    SOB_STRING_SET((SchemeObject*)r[0],r[1],(char)r[2]);
    r_res = (int)&sc_void;
    BI_RETURN();

Lstart:
    /* create closures for the free variables of the built-in procedures */
    CREATE_BUILTIN_CLOS("car"           ,&&Lcar);
    CREATE_BUILTIN_CLOS("cdr"           ,&&Lcdr);
    CREATE_BUILTIN_CLOS("symbol?"       ,&&Lsymbol);
    CREATE_BUILTIN_CLOS("set-car!"      ,&&Lset_car);
    CREATE_BUILTIN_CLOS("set-cdr!"      ,&&Lset_cdr);
    CREATE_BUILTIN_CLOS("make-vector"   ,&&Lmake_vector);
    CREATE_BUILTIN_CLOS("vector-set!"   ,&&Lvector_set);
    CREATE_BUILTIN_CLOS("vector-ref"    ,&&Lvector_ref);
    CREATE_BUILTIN_CLOS("vector-length"    ,&&Lvector_length);
    CREATE_BUILTIN_CLOS("binary=?"      ,&&LbinaryEQ);
    CREATE_BUILTIN_CLOS("binary-add"    ,&&LbinaryADD);
    CREATE_BUILTIN_CLOS("binary-sub"    ,&&LbinarySUB);
    CREATE_BUILTIN_CLOS("binary-mul"    ,&&LbinaryMUL);
    CREATE_BUILTIN_CLOS("binary-div"    ,&&LbinaryDIV);
    CREATE_BUILTIN_CLOS("binary<?"      ,&&LbinaryLT);
    CREATE_BUILTIN_CLOS("box"           ,&&Lbox);
    CREATE_BUILTIN_CLOS("null?"         ,&&Lnull);
    CREATE_BUILTIN_CLOS("char->integer" ,&&Lchar_to_integer);
    CREATE_BUILTIN_CLOS("integer->char" ,&&Linteger_to_char);
    CREATE_BUILTIN_CLOS("apply"         ,&&Lapply);
    CREATE_BUILTIN_CLOS("boolean?"      ,&&Lboolean);
    CREATE_BUILTIN_CLOS("char?"         ,&&Lchar);
    CREATE_BUILTIN_CLOS("cons"          ,&&Lcons);
    CREATE_BUILTIN_CLOS("eq?"           ,&&Leq);
    CREATE_BUILTIN_CLOS("integer?"      ,&&Linteger);
    CREATE_BUILTIN_CLOS("make-string"   ,&&Lmake_string);
    CREATE_BUILTIN_CLOS("number?"       ,&&Linteger); /* same as integer? */
    CREATE_BUILTIN_CLOS("pair?"         ,&&Lpair);
    CREATE_BUILTIN_CLOS("procedure?"    ,&&Lprocedure);
    CREATE_BUILTIN_CLOS("string?"       ,&&Lstring);
    CREATE_BUILTIN_CLOS("vector?"       ,&&Lvector);
    CREATE_BUILTIN_CLOS("zero?"         ,&&Lzero);
    CREATE_BUILTIN_CLOS("remainder"     ,&&Lremainder);
    CREATE_BUILTIN_CLOS("string->symbol",&&Lstring_to_symbol);
    CREATE_BUILTIN_CLOS("symbol->string",&&Lsymbol_to_string);
    CREATE_BUILTIN_CLOS("string-length" ,&&Lstring_length);
    CREATE_BUILTIN_CLOS("string-ref"    ,&&Lstring_ref);
    CREATE_BUILTIN_CLOS("string-set!"   ,&&Lstring_set);

/* </Built-in procedures> */
