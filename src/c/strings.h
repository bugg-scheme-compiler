#ifndef __STRINGS_H
#define __STRINGS_H

/* Error messages for use with ASSERT_ALWAYS( predicate, error_message ) */
#define MSG_ERR_ARGCOUNT(proc,ex) ("incorrect number of arguments for " proc ". Expected " #ex "\n")
#define MSG_ERR_NOTPAIR "not a pair"
#define MSG_ERR_NOTLIST "not a proper list"
#define MSG_ERR_APPNONPROC  "attempt to apply non-procedure"
#define MSG_ERR_DIVZERO "division by zero"
#define MSG_ERR_NDXOUTOFBOUNDS "index out of bounds"

#endif
