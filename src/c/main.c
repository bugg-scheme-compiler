#include "assertions.h"
#include "scheme.h"
#include <stdio.h>

/* Stack (initialized in arch.c) */
int* stack;
int sp;
int fp;

/* General Purpose Registers (initialized in arch.c) */
int* r;
int r_res;

/* Top Level */
SymbolNode *topLevel;

/* Entry point */
int main() {
    topLevel = NULL; /* todo: add build-in procedures */

    schemeCompiledFunction();
    ASSERT_ALWAYS(r_res!=0,"");

    if ( IS_SOB_VOID(r_res) ) goto Lend;
    printf( sobToString(SOB(r_res)) );
Lend:
    return 0;
}
