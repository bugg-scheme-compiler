/* support for the simulated micro-architecture */

#include "arch.h"
#include "assertions.h"
#include <stdlib.h>

int MAX_STACK_SIZE;

void initArchitecture(int stackSize, int regsCount)
{
  MAX_STACK_SIZE = stackSize;
  stack = (int*)malloc( stackSize * sizeof(int) );
  sp = 0;
  fp = 0;

  r = (int*)malloc( regsCount * sizeof(int) );
}

void push(int x)
{
  ASSERT_ALWAYS(sp < MAX_STACK_SIZE,"");

  stack[sp++] = x;
}

int pop()
{
  ASSERT_ALWAYS(sp > 0,"");

  return stack[--sp];
}
