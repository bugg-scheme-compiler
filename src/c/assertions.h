/* assertions.h
 * A custom assertion mechanism for C
 * 
 * Programmer: Mayer Goldberg, 1999
 *
 * Usage: To make use of the following custom assertion mechanism,
 * include this file, and compile, when needed, with the -DDEBUG. Use
 * ASSERT_DEBUG(expr) to assert a condition that must hold while
 * debugging. Use ASSERT_ALWAYS(expr) to assert a condition that must
 * hold always (also while not debugging).  
 */

#ifndef __ASSERTIONS_H__

#define __ASSERTIONS_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef DEBUG

#define ASSERT_DEBUG(e) \
do { \
  if (! (e) ) { \
    fprintf(stderr, \
	    "Failed assertion in file \"%s\", line %d:\n" \
            "  ASSERT_DEBUG(%s);\n", \
	    __FILE__, __LINE__, #e); \
    exit(-1); \
  } \
} while (0)

#else

#define ASSERT_DEBUG(e) do {} while (0)

#endif

#define ASSERT_ALWAYS(e,msg) \
do { \
  if (! (e) ) { \
    fprintf(stderr, \
	    "Failed assertion in file \"%s\", line %d:\n" \
            "  ASSERT_ALWAYS(%s);\n" \
            "  %s\n", \
	    __FILE__, __LINE__, #e, msg); \
    exit(-1); \
  } \
} while (0)

#endif
