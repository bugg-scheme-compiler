/* scheme.h
 * A header file that defines the basic Scheme data types, etc. Used
 * with the scheme->c compiler.
 * Programmer: Mayer Goldberg, 2000
 */

#ifndef __SCHEME_H
#define __SCHEME_H

#ifndef NULL
#define NULL ((void *)0)
#endif

typedef enum SchemeType {
  SCHEME_VOID,
    SCHEME_TYPE_FIRST = SCHEME_VOID,
  SCHEME_INT,
  SCHEME_CHAR,
  SCHEME_BOOL,
  SCHEME_STRING,
  SCHEME_NIL,
  SCHEME_PAIR,
  SCHEME_VECTOR,
  SCHEME_SYMBOL,
  SCHEME_CLOSURE,
  SCHEME_TYPE_END /* not to be used */
} SchemeType;

/* basic support for scheme objects */

typedef struct SchemeIntData {
  int value;
} SchemeIntData;

typedef struct SchemeCharData {
  char value;
} SchemeCharData;

typedef struct SchemeBoolData {
  int value;
} SchemeBoolData;

typedef struct SchemeStringData {
  int length;
  char *value;
} SchemeStringData;

typedef struct SchemePairData {
  struct SchemeObject *car, *cdr;
} SchemePairData;

typedef struct SchemeVectorData {
  int length;
  struct SchemeObject **vec; 
} SchemeVectorData;

typedef struct SchemeSymbolData {
  struct SymbolEntry *sep;
} SchemeSymbolData;

typedef struct SchemeClosureData {
  struct SchemeObject **env;
  void *code;
} SchemeClosureData;

typedef union SchemeObjectData {
  struct SchemeIntData sid;
  struct SchemeCharData scd;
  struct SchemeBoolData sbd;
  struct SchemeStringData ssd;
  struct SchemePairData spd;
  struct SchemeVectorData svd;
  struct SchemeSymbolData smd;
  struct SchemeClosureData scld;
} SchemeObjectData;

typedef struct SchemeObject {
  SchemeType type;
  SchemeObjectData *schemeObjectData;
} SchemeObject;

#define VOID(x)                  ((void *) (x))
#define SOB(x)                   ((SchemeObject *) (x))
#define SOD(x)                   ((SchemeObjectData *) (x))
#define SOB_TYPE(x)              SOB((x))->type
#define SOB_DATA(x)              SOB((x))->schemeObjectData

#define IS_SOB_VOID(x)           (SOB((x))->type == SCHEME_VOID)
#define IS_SOB_INT(x)            (SOB((x))->type == SCHEME_INT)
#define IS_SOB_CHAR(x)           (SOB((x))->type == SCHEME_CHAR)
#define IS_SOB_BOOL(x)           (SOB((x))->type == SCHEME_BOOL)
#define IS_SOB_STRING(x)         (SOB((x))->type == SCHEME_STRING)
#define IS_SOB_NIL(x)            (SOB((x))->type == SCHEME_NIL)
#define IS_SOB_PAIR(x)           (SOB((x))->type == SCHEME_PAIR)
#define IS_SOB_VECTOR(x)         (SOB((x))->type == SCHEME_VECTOR)
#define IS_SOB_SYMBOL(x)         (SOB((x))->type == SCHEME_SYMBOL)
#define IS_SOB_CLOSURE(x)        (SOB((x))->type == SCHEME_CLOSURE)

#define SOB_INT_VALUE(x)           SOB_DATA((x))->sid.value
#define SOB_CHAR_VALUE(x)          SOB_DATA((x))->scd.value
#define SOB_BOOL_VALUE(x)          SOB_DATA((x))->sbd.value
#define SOB_STRING_LENGTH(x)       SOB_DATA((x))->ssd.length
#define SOB_STRING_VALUE(x)        SOB_DATA((x))->ssd.value
#define SOB_STRING_REF(x, i)       SOB_STRING_VALUE((x))[i]
#define SOB_STRING_SET(x, i, c)    SOB_STRING_VALUE((x))[i] = c
#define SOB_PAIR_CAR(x)            SOB_DATA((x))->spd.car
#define SOB_PAIR_CDR(x)            SOB_DATA((x))->spd.cdr
#define SOB_VECTOR_LENGTH(x)       SOB_DATA((x))->svd.length
#define SOB_VECTOR_VALUE(x)        SOB_DATA((x))->svd.vec
#define SOB_VECTOR_REF(x, i)       SOB_VECTOR_VALUE((x))[i]
#define SOB_VECTOR_SET(x, i, sob)  SOB_VECTOR_VALUE((x))[i] = sob
#define SOB_SYMBOL_ENTRY(x)        SOB_DATA((x))->smd.sep
#define SOB_SYMBOL_HAS_VALUE(x)    SOB_SYMBOL_ENTRY((x))->isDefined
#define SOB_SYMBOL_NAME(x)         SOB_SYMBOL_ENTRY((x))->name
#define SOB_SYMBOL_VALUE(x)        SOB_SYMBOL_ENTRY((x))->sob
#define SOB_CLOSURE_ENV(x)         SOB_DATA((x))->scld.env
#define SOB_CLOSURE_CODE(x)        SOB_DATA((x))->scld.code

#define SOB_BOOL_FALSE(x) (IS_SOB_BOOL(x) && (SOB_BOOL_VALUE(x) == 0))

SchemeObject *makeSchemeObject();
SchemeObject *makeSchemeVoid();
SchemeObject *makeSchemeInt(int n);
SchemeObject *makeSchemeChar(char c);
SchemeObject *makeSchemeBool(int b);
SchemeObject *makeSchemeString(int n, char c);
SchemeObject *makeSchemeStringFromCString(char *s);
SchemeObject *makeSchemeNil();
SchemeObject *makeSchemePair(SchemeObject *car, SchemeObject *cdr);
SchemeObject *makeSchemeVectorInit(int n, SchemeObject *initSob);
SchemeObject *makeSchemeSymbol(char *s);
SchemeObject *makeSchemeClosure(void* env,void* code);

char *sobToString(SchemeObject *sob);

/* I am too lazy to think up a good hash function, so I'm not using a
 * hash table but rather a binary tree. Of course, the performance for
 * free variable look up will not be as good, but the implementation
 * details are hidden behind an API, and can therefore be changed
 * later.  
 */

typedef struct SymbolEntry {
  char *name;
  int isDefined;
  SchemeObject *sob;
} SymbolEntry;

typedef struct SymbolNode {
  SymbolEntry *sym;
  struct SymbolNode *left, *right;
} SymbolNode;

#define SYM_ENTRY(symNode)     ((SymbolNode *)symNode)->sym
#define SYM_NAME(symNode)      SYM_ENTRY((symNode))->name
#define SYM_HAS_VALUE(symNode) SYM_ENTRY((symNode))->isDefined
#define SYM_VALUE(symNode)     SYM_ENTRY((symNode))->sob
#define SYM_LEFT(symNode)      ((symNode))->left
#define SYM_RIGHT(symNode)     ((symNode))->right

SymbolEntry *probeSymbolDefined(char *name, SymbolNode *t);
SymbolEntry *getSymbol(char *name, SymbolNode *t);

/* this is intended for future support for the garbage collector */

void *autoMalloc(int n);
void autoFree(void *p);

/* this is the name of the function created by our compiler */

void schemeCompiledFunction();

#endif
