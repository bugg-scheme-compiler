/* Run-Time Enviroment Manager */

#ifndef __RTEMGR_H
#define __RTEMGR_H

#include "scheme.h"

int** extendEnviroment(int** currenv, int currenv_size);
void shiftActFrmDown();
void shiftStackUp(int pos, unsigned int amount);
void shiftStackDown(int pos, unsigned int amount);
void prepareStackForAbsOpt(int formalParams);
SchemeObject* reverseSchemeList( SchemeObject* list );
void pushArgsList(SchemeObject* list);

#define PUSH_INITIAL_ACTFRM() \
  push(0);         /* no arguments */ \
  push((int)NULL); /* no enviroment */ \
  push((int)NULL); /* no return address (yet. will be set later) */ \
  push(fp); \
  fp = sp;

#define POP_INITIAL_ACTFRM() \
  fp = pop();

#endif
