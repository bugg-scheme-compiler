/* Architecture */

#ifndef __ARCH_H
#define __ARCH_H

void initArchitecture(int stackSize, int regsCount);

/* Stack */
extern int* stack;
extern int sp;
extern int fp;

void push(int x);
int pop();

/* The stack **WHILE IN A USER PROCEDURE**:
        fp -> |      |
              | fp   |  - the old fp (before this application)
              | ret  |  - return address
              | env  |  - points to the enviroment vector
              | n    |  - number of arguments
              | A0   |  - argument 0
              | A1   |  - argument 1
              | ...  |  - ...
 ST_FRMEND -> | An-1 |  - argument n-1

   Note: ST_FRMEND points to the end of the current actvation frame (in fact,
         to the last element: An-1). Note that ST_FRMEND does not have to be
         equal to [the old fp + 1] (in case we are in the middle of an
         application and already pushed some arguments onto the stack).

   Note: sp does NOT have to be equal to fp - for example if we are in the
         middle of applying a procedure and already pushed some arguments
         on to the stack (then sp has changed, but fp has not).

   Macros for user procedures:
*/
#define ST_ARG(n) (stack[fp-5-(n)])
#define ST_ARG_COUNT() (stack[fp-4])
#define ST_OLDFP() (stack[fp-1])
#define ST_RET() (stack[fp-2])
#define ST_ENV() (stack[fp-3])
#define ST_FRMEND() (fp-4-stack[fp-4])
#define RETURN() goto *pop()

/* General Purpose Registers */
extern int* r;
extern int r_res;

#endif
