(* Tag Parser *)

signature TAG_PARSER =
sig
    val stringToPE : string -> Expr;
    val stringToPEs : string -> Expr list;
end;

(* *********************************************************** Tag Parser *)

exception ErrorReservedWordUsedImproperly of string;
exception ErrorMalformedLambdaArgList of string;
exception UnrecognizedAST of Sexpr;
exception NotASymbol of Sexpr;

structure TagParser : TAG_PARSER = 
struct
val reservedSymbols = ["and", "begin", "cond", "define", "else",
               "if", "lambda", "let", "let*", "letrec",
               "or", "quote", "set!"];

fun reservedWord(str) = 
    ormap (fn rs => (String.compare(rs, str) = EQUAL)) reservedSymbols;

fun scmRdcAndRac(Nil, retRdcRac) = retRdcRac(Nil, Nil)
  | scmRdcAndRac(Pair(car, cdr), retRdcRac) = 
    scmRdcAndRac(cdr, (fn (rdc, rac) =>
              retRdcRac(Pair(car, rdc), rac)))
  | scmRdcAndRac(e, retRdcRac) = retRdcRac(Nil, e);

fun parseExpr(Void) = Const(Void)
  | parseExpr(Nil) = Const(Nil)
  | parseExpr(e as Number(_)) = Const(e)
  | parseExpr(e as Char(_)) = Const(e)
  | parseExpr(e as Bool(_)) = Const(e)
  | parseExpr(e as String(_)) = Const(e)
  | parseExpr(Pair(Symbol("quote"),Pair(e, Nil))) = Const(e)
  | parseExpr(Symbol(e)) =
        if reservedWord(e) then
            raise ErrorReservedWordUsedImproperly(e)
        else
            Var(e)
  | parseExpr(Pair(Symbol("if"),Pair(test,Pair(dit, Nil)))) =
        If(parseExpr(test), parseExpr(dit), Const(Void))
  | parseExpr(Pair(Symbol("if"),Pair(test,Pair(dit,Pair(dif, Nil))))) =
        If(parseExpr(test), parseExpr(dit), parseExpr(dif))

  | parseExpr(sexpr as Pair(Symbol("lambda"),Pair(argl,body))) =
    (* (lambda <argl> se)
       or
       (lambda <argl> se1 se2 ...
       [lambda with implicit sequence] *)
        parseImplicitSeq(body,
            (fn (pe) => parseAbs(argl,pe)),
            (fn () => raise UnrecognizedAST(sexpr)))

  | parseExpr(Pair(Symbol("define"),Pair(Symbol(s),Pair(se,Nil)))) =
    (* (define <symbol> <expr>) *)
        Def( Var(s), parseExpr(se) )

  | parseExpr(sexpr as Pair(Symbol("define"),
                   Pair(Pair(Symbol(s),argl),
                        body))) =
    (* (define (<symbol> [argl]) <expr>)
       or:
       (define (<symbol> [argl]) <expr> ...
       [define with implicit sequence] *)
        parseImplicitSeq(body,
            (fn (pe) => Def( Var(s), parseAbs(argl,pe) )),
            (fn () => raise UnrecognizedAST(sexpr)))

  | parseExpr(Pair(Symbol("set!"),Pair(Symbol(s),Pair(sexpr,Nil)))) =
    (* (set! <symbol> <expr>) *)
        Set( Var(s), parseExpr(sexpr) )

  | parseExpr(orexpr as Pair(Symbol("or"),sexprs)) =
    (* (or <expr> ... *)
        let
            val peList = parseExprs( schemeListToML(sexprs) )
            handle NotAList(_) =>
                raise UnrecognizedAST(orexpr)
        in
            case peList of
                []   => Const(Bool(false))
              | [pe] => pe
              | _    => Or(peList)
        end

  | parseExpr(andexpr as Pair(Symbol("and"),sexprs)) =
    (* (and <expr> ... *)
        let
            val peList = parseExprs(schemeListToML(sexprs))
            handle NotAList(_) => raise UnrecognizedAST(andexpr)
        in
            expandAnd( peList )
        end

  | parseExpr(condexpr as Pair(Symbol("cond"),Nil)) =
        raise ErrorReservedWordUsedImproperly("cond")

  | parseExpr(condexpr as Pair(Symbol("cond"),condpairs)) =
    (* (cond ... *)
        let
            val pairs = schemeListToML(condpairs)
            handle NotAList(_) => raise UnrecognizedAST(condexpr)
        in
            parseCondPairs( pairs )
        end

  | parseExpr(letexpr as Pair(Symbol("let"),Pair(bindings,body))) =
    (* (let <bindings> <body>) *)
        let
            val bindingsList = schemeListToML(bindings)
            handle NotAList(_) => raise UnrecognizedAST(letexpr)
        in
            parseImplicitSeq(body,
                (fn (pe) =>
                    parseLetBindings(bindingsList,
                        (fn (names,values) =>
                            App( Abs(names,pe), parseExprs(values) )),
                        (fn () => App( Abs([],pe),[])))),
                (fn () =>
                    raise ErrorReservedWordUsedImproperly("let")))
        end

  | parseExpr(letstarexpr as Pair(Symbol("let*"),Pair(bindings,body))) =
    (* (let* <bindings> <body>) *)
        let
            val bindingsList = schemeListToML(bindings)
            handle NotAList(_) => raise UnrecognizedAST(letstarexpr)
        in
            parseExpr( expandLetStar(bindingsList,body) )
        end

  | parseExpr(letrecexpr as Pair(Symbol "letrec",Pair(bindings,body))) =
    (* (letrec <bindings> <body>) *)
        let
            val bindingsList = schemeListToML(bindings)
            handle NotAList(_) => raise UnrecognizedAST(letrecexpr)
        in
            expandLetRec(bindingsList,body)
        end

  | parseExpr(Pair(Symbol "begin",seq)) =
        parseImplicitSeq(seq,
            (fn (pe) => pe),
            (fn () => raise ErrorReservedWordUsedImproperly("begin")))

  | parseExpr(sexpr as Vector(_)) =
        Const(sexpr)

  | parseExpr (e as Pair(p, q)) =
        scmRdcAndRac(q,
            (fn (rdc, Nil) =>
                App(parseExpr(p), map parseExpr(schemeListToML(rdc)))
              | _ =>
                raise UnrecognizedAST(e)))

and parseLetBindings([],retNamesAndValues,retNone) = retNone()

  | parseLetBindings([Pair(Symbol(name),Pair(value,Nil))],
    retNamesAndValues, retNone) =
        retNamesAndValues([name],[value])

  | parseLetBindings(Pair(Symbol(name),Pair(value,Nil)) :: rest,
    retNamesAndValues, retNone) =
        parseLetBindings(rest,
            (fn (names,values) =>
                retNamesAndValues(name :: names,value :: values)),
            (fn () => retNone()))

  | parseLetBindings(sexpr :: _,_,_) = raise UnrecognizedAST(sexpr)

and parseCondPairs([]) = Const(Void)
  | parseCondPairs([Pair(Symbol("else"),exprs)]) =
        parseImplicitSeq(exprs,
            (fn (pe) => pe),
            (fn () => raise ErrorReservedWordUsedImproperly("else")))
  | parseCondPairs(Pair(Symbol("else"),_) :: _) =
        raise ErrorReservedWordUsedImproperly("cond")
  | parseCondPairs(Pair(test,exprs) :: restpairs) =
        parseImplicitSeq(exprs,
            (fn (pe) => If( parseExpr(test), pe, parseCondPairs(restpairs) )),
(*            (fn () => raise ErrorReservedWordUsedImproperly("cond"))) *)
            (fn () => Or [parseExpr(test),parseCondPairs(restpairs)] ))
  | parseCondPairs(_) =
        raise ErrorReservedWordUsedImproperly("cond")

and expandAnd([]) = Const(Bool(true))
  | expandAnd([pe]) = pe
  | expandAnd([pe1,pe2]) = If( pe1, expandAnd([pe2]), Const(Bool(false)) )
  | expandAnd(pe :: rest) = If( pe, expandAnd(rest), Const(Bool(false)) )

and expandLetStar([],body) =
        Pair(Symbol "let",Pair(Nil,body))
  | expandLetStar([pair],body) =
        Pair(Symbol "let",Pair(Pair(pair,Nil),body))
  | expandLetStar(pair :: rest,body) =
    (* [(x 4),...], body *)
        Pair(Symbol "let",Pair(Pair(pair,Nil),Pair(expandLetStar(rest,body),Nil)))

    (* Expands
         (letrec ((n1 v1)
                  ...
                  (nk vk))
            e1 .. en)
       to
         (let ((n1 #f)
               ...
               (nk #f))
            (set! n1 v1)
            ...
            (set! nk vk)
            (let ()
              e1 .. en))
    *)
and expandLetRec(bindingsList,Nil) =
        raise ErrorReservedWordUsedImproperly("letrec")
  | expandLetRec(bindingsList,body) =
        parseLetBindings(bindingsList,
            (fn (names,values) =>
                let
                    val newBindings = MLListToScheme(map makeInitPair names)
                    val setPairs = makeSetPairs(names,values)
                in
                    parseExpr(Pair(Symbol "let",Pair(newBindings,
                            combine(MLListToScheme(setPairs),
                                Pair(Pair(Symbol "let", Pair(Nil,body)),Nil)
                            ))))
                end),
            (fn () => parseExpr(Pair (Symbol "let", Pair (Nil,body)))))

and makeInitPair(name) = Pair(Symbol name,Pair(Bool false,Nil))

and makeSetPairs([],[]) = []
  | makeSetPairs(name :: restNames, value :: restValues) =
        MLListToScheme( [Symbol "set!",Symbol name, value] ) ::
        makeSetPairs(restNames,restValues)
  | makeSetPairs(_,_) = raise UnrecognizedAST(Void)

and parseImplicitSeq(schemeList,retPE,retNone) =
    let
        val peList = (map parseExpr (schemeListToML schemeList))
        handle NotAList(_) =>
            raise UnrecognizedAST(schemeList)
    in
        case peList of
            []   => retNone()
          | [pe] => retPE(pe)
          | _    => retPE( Seq peList )
    end

and parseAbs(argl,body) =
        case argl of
            Pair(_,_) =>
            (* argl is either simple: (<symbol> ... <symbol>)
                         or optional: (<symbol> ... <symbol> . <symbol>) *)
            parseAbsArgsList(argl,
                (fn (argsNames) =>
                    Abs(argsNames,body)),
                (fn (argsNames,optArgsName) =>
                    AbsOpt(argsNames,optArgsName,body)))
            
          | Symbol(s) =>
            (* argl is variadic: <symbol> *)
            if reservedWord(s) then
                raise ErrorReservedWordUsedImproperly(s)
            else
                AbsVar(s, body )
            
          | Nil =>
            (* no args at all *)
            Abs([],body)
            
          | _ =>
            raise ErrorMalformedLambdaArgList("not a var/list/improper-list")

and parseAbsArgsList(sexpr,retArgsNames,retArgsNamesAndOptsName) =
    case sexpr of
        Pair(Symbol(argName),Nil) =>
        if reservedWord(argName) then
            raise ErrorReservedWordUsedImproperly(argName)
        else
            retArgsNames([argName])
        
      | Pair(Symbol(argName),Symbol(optsName)) =>
        if reservedWord(argName) then
            raise ErrorReservedWordUsedImproperly(argName)
        else
            if reservedWord(optsName) then
                raise ErrorReservedWordUsedImproperly(optsName)
            else
                retArgsNamesAndOptsName([argName],optsName)
        
      | Pair(Symbol(argName),rest) =>
        if reservedWord(argName) then
            raise ErrorReservedWordUsedImproperly(argName)
        else
            parseAbsArgsList(rest,
                (fn (argsNames) =>
                    retArgsNames(argName :: argsNames)),
                (fn (argsNames,optsName) =>
                    retArgsNamesAndOptsName(argName :: argsNames,optsName)))
        
      | _ =>
        raise ErrorMalformedLambdaArgList("not a list/improper-list")

and parseExprs([]) = []
  | parseExprs(sexpr :: rest) = parseExpr(sexpr) :: parseExprs(rest);

fun stringToPE string = parseExpr(Reader.stringToSexpr string);

fun stringToPEs string = parseExprs( Reader.stringToSexprs string );

end; (* of structure TagParser *)
