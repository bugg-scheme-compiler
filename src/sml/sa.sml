(* Semantic Analysis *)

signature SEMANTIC_ANALYSIS =
sig
    val removeNestedDefine : Expr -> Expr;
    val boxSet : Expr -> Expr;
    val annotateTC : Expr -> Expr;
    val lexicalAddressing : Expr -> Expr;
    val semanticAnalysis : Expr -> Expr;
end;

(* ******************************************************* Semantic Analysis *)

(* union any number of lists *)
fun union [set, []] = set
  | union [set, (x :: rest)] =
        if not (List.exists (fn em => em=x) set) then
            union [(x :: set), rest]
        else
            union [set, rest]
  | union (A :: (B :: rest)) = union [A, union (B :: rest)]
  | union [A] = A
  | union [] = [];

(* intersect any number of lists *)
fun intersect [set,[]] = []
  | intersect [set, (x :: rest)] =
        if List.exists (fn em => em=x) set then
            intersect [set,rest] @ [x]
        else
            intersect [set,rest]
  | intersect (A :: (B :: rest)) = intersect [A, intersect (B :: rest)]
  | intersect [A] = A
  | intersect [] = [];

(* (subtract A B) <==> A-B *)
fun subtract set [] = set
  | subtract set (x :: rest) =
        subtract (List.filter (fn em => em<>x) set) rest;

(* (isMember e) S = true iff e is in S *)
fun isMember e = List.exists (fn e' => e'=e);

exception ErrorEmptyBody;
exception ErrorNotAVar;
exception ErrorInvalidDefineContext;

structure SemanticAnalysis : SEMANTIC_ANALYSIS =
struct

(* Keeps only first-level Seq objects, dissolves the inner ones *)
fun flattenSeq(Seq [e]) = e
  | flattenSeq(Seq(cnt)) = Seq(dissolveSeq cnt)
  | flattenSeq(If(p,c,a)) = If(flattenSeq(p),flattenSeq(c),flattenSeq(a))
  | flattenSeq(Abs(p,b)) = Abs(p,flattenSeq(b))
  | flattenSeq(AbsOpt(p,opt,b)) = AbsOpt(p,opt,flattenSeq(b))
  | flattenSeq(AbsVar(opt,b)) = AbsVar(opt,flattenSeq(b))
  | flattenSeq(App(proc,params)) = App(flattenSeq(proc), map flattenSeq params)
  | flattenSeq(AppTP(proc,params)) = AppTP(flattenSeq(proc), map flattenSeq params)
  | flattenSeq(Or preds) = Or( map flattenSeq preds )
  | flattenSeq(Set(v,e)) = Set(v,flattenSeq(e))
  | flattenSeq(Def(v,e)) = Def(v,flattenSeq(e))
  | flattenSeq(x) = x
(* Recursively replaces any Seq(x) with x *)
and dissolveSeq(Seq(cnt) :: rest) = dissolveSeq(cnt) @ dissolveSeq(rest)
  | dissolveSeq(ex :: rest) = flattenSeq(ex) :: dissolveSeq(rest)
  | dissolveSeq([]) = [];

(* Keeps only top-level Define objects, and
   expands the inner ones to letrec.
   Inner Define objects must be first in the sequence of statements.

   top-level Define objects: Define objects that affect the global enviroment
*)
fun removeNestedDefine'(e as Abs(_,Def(_,_))) = raise ErrorEmptyBody
  | removeNestedDefine'(e as AbsOpt(_,_,Def(_,_))) = raise ErrorEmptyBody
  | removeNestedDefine'(e as AbsVar(_,Def(_,_))) = raise ErrorEmptyBody
  | removeNestedDefine'(Abs(p,Seq (cnt as (Def(_,_) :: _)))) = Abs(p,expandDefines(cnt,[],[]))
  | removeNestedDefine'(AbsOpt(p,opt,Seq (cnt as (Def(_,_) :: _)))) = AbsOpt(p,opt,expandDefines(cnt,[],[]))
  | removeNestedDefine'(AbsVar(opt,Seq (cnt as (Def(_,_) :: _)))) = AbsVar(opt,expandDefines(cnt,[],[]))
  | removeNestedDefine'(Abs(p,b)) = Abs(p,removeNestedDefine'(b))
  | removeNestedDefine'(AbsOpt(p,opt,b)) = AbsOpt(p,opt,removeNestedDefine'(b))
  | removeNestedDefine'(AbsVar(opt,b)) = AbsVar(opt,removeNestedDefine'(b))
  | removeNestedDefine'(If(p,c,a)) = If(removeNestedDefine'(p),removeNestedDefine'(c),removeNestedDefine'(a))
  | removeNestedDefine'(App(proc,params)) = App(removeNestedDefine'(proc),map removeNestedDefine' params)
  | removeNestedDefine'(AppTP(proc,params)) = AppTP(removeNestedDefine'(proc),map removeNestedDefine' params)
  | removeNestedDefine'(Seq(cnt)) = Seq(map removeNestedDefine' cnt)
  | removeNestedDefine'(Or preds) = Or(map removeNestedDefine' preds)
  | removeNestedDefine'(Set(v,e)) = Set(v,removeNestedDefine'(e))
  | removeNestedDefine'(Def(v,e)) = Def(v,removeNestedDefine'(e))
  | removeNestedDefine'(x) = x
and expandDefines([],_,_) = raise ErrorEmptyBody
  | expandDefines(Def(v,e) :: rest,vars,vals) = expandDefines(rest,v :: vars, e :: vals)
  | expandDefines(body,vars,vals) =
        makeLetRec(extractVarNames(List.rev(vars)),
                   List.rev(vals),
                   map removeNestedDefine' body)
and makeLetRec(vars,vals,body) =
        App(Abs(vars,Seq(makeSetExprs(vars,vals) @ body)),map makeFalse vals)
and makeSetExprs([],_) = []
  | makeSetExprs(_,[]) = []
  | makeSetExprs(v :: restVars, e :: restVals) = Set(Var(v),removeNestedDefine'(e)) :: makeSetExprs(restVars,restVals)
and makeFalse(_) = Const(Bool false)
and extractVarNames([]) = []
  | extractVarNames(Var(name) :: rest) = name :: extractVarNames(rest)
  | extractVarNames(_) = raise ErrorNotAVar
and removeNestedDefine x = removeNestedDefine' (flattenSeq x);

(* Raises an error if there is a Define object with an invalid context (such as
   a Define inside an Abs) *)
fun checkDefineContexts(If(p,c,a)) = If(checkDefineContexts(p),checkDefineContexts(c),checkDefineContexts(a))
  | checkDefineContexts(Abs(p,b)) = Abs(p,raiseErrorOnDefine(b))
  | checkDefineContexts(AbsOpt(p,opt,b)) = AbsOpt(p,opt,raiseErrorOnDefine(b))
  | checkDefineContexts(AbsVar(opt,b)) = AbsVar(opt,raiseErrorOnDefine(b))
  | checkDefineContexts(App(proc,params)) = App(checkDefineContexts(proc),map checkDefineContexts params)
  | checkDefineContexts(AppTP(proc,params)) = AppTP(checkDefineContexts(proc),map checkDefineContexts params)
  | checkDefineContexts(Seq cnt) = Seq( map checkDefineContexts cnt )
  | checkDefineContexts(Or preds) = Or( map checkDefineContexts preds )
  | checkDefineContexts(Set(v,e)) = Set(v,checkDefineContexts(e))
  | checkDefineContexts(Def(v,e)) = Def(v,checkDefineContexts(e))
  | checkDefineContexts(x) = x
and raiseErrorOnDefine(Def(_,_)) = raise ErrorInvalidDefineContext
  | raiseErrorOnDefine(If(p,c,a)) = If(raiseErrorOnDefine(p),raiseErrorOnDefine(c),raiseErrorOnDefine(a))
  | raiseErrorOnDefine(Abs(p,b)) = Abs(p,raiseErrorOnDefine(b))
  | raiseErrorOnDefine(AbsOpt(p,opt,b)) = AbsOpt(p,opt,raiseErrorOnDefine(b))
  | raiseErrorOnDefine(AbsVar(opt,b)) = AbsVar(opt,raiseErrorOnDefine(b))
  | raiseErrorOnDefine(App(proc,params)) = App(raiseErrorOnDefine(proc),map raiseErrorOnDefine params)
  | raiseErrorOnDefine(AppTP(proc,params)) = AppTP(raiseErrorOnDefine(proc),map raiseErrorOnDefine params)
  | raiseErrorOnDefine(Seq cnt) = Seq( map raiseErrorOnDefine cnt )
  | raiseErrorOnDefine(Or preds) = Or( map raiseErrorOnDefine preds )
  | raiseErrorOnDefine(Set(v,e)) = Set(v,raiseErrorOnDefine(e))
  | raiseErrorOnDefine(x) = x;

(* Annotates tail applications.
   Takes an Expr as an argument and returns an Expr, in which all
   App-expressions that are in tail position have been replaced with
   corresponding AppTP-expressions.
*)
datatype Context = Head | Tail;
fun annotateInContext context (If(test,dit,dif)) =
        If((annotateInContext Head test),(annotateInContext context dit),(annotateInContext context dif))
  | annotateInContext context (Seq []) = Seq []
  | annotateInContext context (Seq cnt) =
        let
            val last :: rest = List.rev cnt
            val annotatedLast = annotateInContext context last
            val annotatedRest = map (annotateInContext Head) rest
        in
            Seq(List.rev( annotatedLast :: annotatedRest ))
        end
  | annotateInContext context (Or []) = Or []
  | annotateInContext context (Or preds) =
        let
            val last :: rest = List.rev preds
            val annotatedLast = annotateInContext context last
            val annotatedRest = map (annotateInContext Head) rest
        in
            Or(List.rev( annotatedLast :: annotatedRest ))
        end
  | annotateInContext _ (Set(n,v)) = Set( (annotateInContext Head n), (annotateInContext Head v) )
  | annotateInContext _ (Def(n,v)) = Def( (annotateInContext Head n), (annotateInContext Head v) )
  | annotateInContext Head (App(proc,args)) = App( (annotateInContext Head proc), map (annotateInContext Head) args )
  | annotateInContext Tail (App(proc,args)) = AppTP( (annotateInContext Head proc), map (annotateInContext Head) args )
  | annotateInContext _ (Abs(p,b)) = Abs(p, annotateInContext Tail b)
  | annotateInContext _ (AbsOpt(p,opt,b)) = AbsOpt(p,opt,annotateInContext Tail b)
  | annotateInContext _ (AbsVar(opt,b)) = AbsVar(opt, annotateInContext Tail b)
  | annotateInContext _ x = x
and annotateTC(x) = annotateInContext Tail x;

(* Autoboxing.
   Takes an Expr as an argument and returns an Expr, in which all variables that
   have both a bound occurrence and are set in a set!-expression are boxed.
   Boxing means
    * Copying the variable from the stack to the heap, right under the
      lambda-expression that deﬁnes the variable
    * Changing all get operations to unbox
    * Changing all set operations to box-set
*)
(* Returns the variables from V (arg1) that are set somewhere in arg2 *)
fun filterSetVars V (Var n) = []
  | filterSetVars V (VarFree n) = filterSetVars V (Var n)
  | filterSetVars V (VarBound (n,_,_)) = filterSetVars V (Var n)
  | filterSetVars V (VarParam (n,_)) = filterSetVars V (Var n)
  | filterSetVars V (Const _) = []
  | filterSetVars V (If (test,dit,dif)) = union (map (filterSetVars V) [test,dit,dif])
  | filterSetVars V (Abs (P,b)) = filterSetVars (subtract V P) b
  | filterSetVars V (AbsOpt (P,opt,b)) = filterSetVars V (Abs (P @ [opt],b))
  | filterSetVars V (AbsVar (opt,b)) = filterSetVars V (Abs ([opt],b))
  | filterSetVars V (App (proc,args)) = union (map (filterSetVars V) (proc :: args))
  | filterSetVars V (AppTP (proc,args)) = filterSetVars V (App (proc,args))
  | filterSetVars V (Seq cnt) = union (map (filterSetVars V) cnt)
  | filterSetVars V (Or preds) = union (map (filterSetVars V) preds)
  | filterSetVars V (Set (Var n,e)) =
        if isMember n V then union [[n],filterSetVars V e]
                        else filterSetVars V e
  | filterSetVars V (Set (VarFree n,v)) = filterSetVars V (Set (Var n,v))
  | filterSetVars V (Set (VarBound (n,_,_),v)) = filterSetVars V (Set (Var n,v))
  | filterSetVars V (Set (VarParam (n,_),v)) = filterSetVars V (Set (Var n,v))
  | filterSetVars V (Set (n,v)) = union (map (filterSetVars V) [n,v])
  | filterSetVars V (Def (n,v)) = union (map (filterSetVars V) [n,v])
(* Returns the free variables in arg2. arg1 is the list of already-bound vars *)
and freeVarsIn' BV (Var n) = if isMember n BV then [] else [n]
  | freeVarsIn' BV (VarFree n) = freeVarsIn' BV (Var n)
  | freeVarsIn' BV (VarBound (n,_,_)) = freeVarsIn' BV (Var n)
  | freeVarsIn' BV (VarParam (n,_)) = freeVarsIn' BV (Var n)
  | freeVarsIn' BV (Const _) = []
  | freeVarsIn' BV (If (test,dit,dif)) = union (map (freeVarsIn' BV) [test,dit,dif])
  | freeVarsIn' BV (Abs (P,b)) = freeVarsIn' (union [BV,P]) b
  | freeVarsIn' BV (AbsOpt (P,opt,b)) = freeVarsIn' BV (Abs (P @ [opt],b))
  | freeVarsIn' BV (AbsVar (opt,b)) = freeVarsIn' BV (Abs ([opt],b))
  | freeVarsIn' BV (App (proc,args)) = union (map (freeVarsIn' BV) (proc :: args))
  | freeVarsIn' BV (AppTP (proc,args)) = freeVarsIn' BV (App (proc,args))
  | freeVarsIn' BV (Seq cnt) = union (map (freeVarsIn' BV) cnt)
  | freeVarsIn' BV (Or preds) = union (map (freeVarsIn' BV) preds)
  | freeVarsIn' BV (Set (n,v)) = union (map (freeVarsIn' BV) [n,v])
  | freeVarsIn' BV (Def (n,v)) = union (map (freeVarsIn' BV) [n,v])
and freeVarsIn x = freeVarsIn' [] x
(* Returns the vars of arg1 that have bound-occurrences in arg2.
   p has a bound-occurrence in b <==> p has a free-occurrence in an
   inner-lambda in b *)
and filterBoundVars P (e as Abs (_,_)) = intersect [(freeVarsIn e),P]
  | filterBoundVars P (e as AbsOpt (_,_,_)) = intersect [(freeVarsIn e),P]
  | filterBoundVars P (e as AbsVar (_,_)) = intersect [(freeVarsIn e),P]
  | filterBoundVars P (If (test,dit,dif)) = union (map (filterBoundVars P) [test,dit,dif])
  | filterBoundVars P (App (proc,args)) = union (map (filterBoundVars P) (proc :: args))
  | filterBoundVars P (AppTP (proc,args)) = filterBoundVars P (App (proc,args))
  | filterBoundVars P (Seq cnt) = union (map (filterBoundVars P) cnt)
  | filterBoundVars P (Or cnt) = union (map (filterBoundVars P) cnt)
  | filterBoundVars P (Set (n,v)) = union (map (filterBoundVars P) [n,v])
  | filterBoundVars P (Def (n,v)) = union (map (filterBoundVars P) [n,v])
  | filterBoundVars _ _ = []
(* Returns the vars from arg1 that need to be boxed in arg2 *)
and filterParamsToBox P b = intersect [(filterBoundVars P b), (filterSetVars P b)]
and makeBoxing name = Set (Var name,App (Var "box", [Var name]))
and makeBoxRead name = App (Var "vector-ref", [Var name,Const (Number 0)])
and makeBoxWrite name ex = App (Var "vector-set!", [Var name,Const (Number 0),ex])
(* Traverses the expression arg2 and replaces
    Var x     with (vector-ref x 0)
    Set (x,v) with (vector-set! x 0 v)
   for each x in arg1 *)
and boxAccesses [] x = x
  | boxAccesses V (Set (Var n,e)) =
        if isMember n V then makeBoxWrite n (boxAccesses V e)
                        else Set (Var n, boxAccesses V e)
  | boxAccesses V (Var n) =
        if isMember n V then makeBoxRead n
                        else Var n
  | boxAccesses V (Abs (P,b)) = Abs(P,boxAccesses (subtract V P) b)
  | boxAccesses V (AbsOpt (P,opt,b)) = AbsOpt(P,opt,boxAccesses (subtract V (opt :: P)) b)
  | boxAccesses V (AbsVar (opt,b)) = AbsVar(opt, boxAccesses (subtract V [opt]) b)
  | boxAccesses V (If (test,dit,dif)) = If (boxAccesses V test,boxAccesses V dit,boxAccesses V dif)
  | boxAccesses V (App (proc,args)) = App (boxAccesses V proc, map (boxAccesses V) args)
  | boxAccesses V (Seq cnt) = Seq (map (boxAccesses V) cnt)
  | boxAccesses V (Or preds) = Or (map (boxAccesses V) preds)
  | boxAccesses V (Set (v,e)) = Set (boxAccesses V v,boxAccesses V e)
  | boxAccesses V (Def (v,e)) = Def (boxAccesses V v,boxAccesses V e)
  | boxAccesses _ e = e
and isAbs (Abs _) = true
  | isAbs (AbsOpt _) = true
  | isAbs (AbsVar _) = true
  | isAbs _ = false
and boxSet (Abs (P,b)) =
        let
            val needBoxing = filterParamsToBox P b
            val boxingExprs = map makeBoxing needBoxing
            val fixedBody = boxAccesses needBoxing b
            val newBody = Seq (boxingExprs @ [fixedBody])
        in Abs (P,flattenSeq (boxSet newBody)) end
  | boxSet (AbsOpt (P,opt,b)) =
        let
            val needBoxing = filterParamsToBox (opt :: P) b
            val boxingExprs = map makeBoxing needBoxing
            val fixedBody = boxAccesses needBoxing b
            val newBody = Seq (boxingExprs @ [fixedBody])
        in AbsOpt (P,opt,flattenSeq (boxSet newBody)) end
  | boxSet (AbsVar (opt,b)) =
        let
            val needBoxing = filterParamsToBox [opt] b
            val boxingExprs = map makeBoxing needBoxing
            val fixedBody = boxAccesses needBoxing b
            val newBody = Seq (boxingExprs @ [fixedBody])
        in AbsVar (opt,flattenSeq (boxSet newBody)) end
  | boxSet (If (test,dit,dif)) = If (boxSet test,boxSet dit,boxSet dif)
  | boxSet (App (proc,args)) = App (boxSet proc,map boxSet args)
  | boxSet (AppTP (proc,args)) = AppTP (boxSet proc,map boxSet args)
  | boxSet (Seq cnt) = Seq (map boxSet cnt)
  | boxSet (Or preds) = Or (map boxSet preds)
  | boxSet (Set (n,v)) = Set (boxSet n,boxSet v)
  | boxSet (Def (n,v)) = Def (boxSet n,boxSet v)
  | boxSet x = x;

fun incMajors bounds = map (fn VarBound(name,major,minor) => VarBound(name,major+1,minor)) bounds
and makeBoundVars params = map (fn VarParam(name,ndx) => VarBound(name,0,ndx)) params
and makeParamVars [] _ = []
  | makeParamVars (name :: rest) ndx = VarParam(name,ndx) :: (makeParamVars rest (ndx+1))
and lookupVar varname [] = VarFree(varname)
  | lookupVar varname (VarParam(name,ndx) :: rest) =
        if varname=name then VarParam(name,ndx)
        else lookupVar varname rest
  | lookupVar varname (VarBound(name,major,minor) :: rest) =
        if varname=name then VarBound(name,major,minor)
        else lookupVar varname rest
  | lookupVar varname (_ :: rest) = lookupVar varname rest
and annotateVars bounds params (Abs(p,b)) =
        Abs(p, annotateVars ((makeBoundVars params) @ (incMajors bounds)) (makeParamVars p 0) b)
  | annotateVars bounds params (AbsOpt(p,opt,b)) =
        AbsOpt(p, opt, annotateVars ((makeBoundVars params) @ (incMajors bounds)) (makeParamVars (p @ [opt]) 0) b)
  | annotateVars bounds params (AbsVar(opt,b)) =
        AbsVar(opt, annotateVars ((makeBoundVars params) @ (incMajors bounds)) (makeParamVars [opt] 0) b)
  | annotateVars bounds params (If(test,dit,dif)) =
        If( (annotateVars bounds params test), (annotateVars bounds params dit), (annotateVars bounds params dif) )
  | annotateVars bounds params (App(proc,args))   = App( (annotateVars bounds params proc), map (annotateVars bounds params) args )
  | annotateVars bounds params (AppTP(proc,args)) = AppTP( (annotateVars bounds params proc), map (annotateVars bounds params) args )
  | annotateVars bounds params (Seq(cnt)) = Seq( map (annotateVars bounds params) cnt )
  | annotateVars bounds params (Or(preds)) = Or( map (annotateVars bounds params) preds )
  | annotateVars bounds params (Set(n,v)) = Set( (annotateVars bounds params n), (annotateVars bounds params v) )
  | annotateVars bounds params (Def(n,v)) = Def( (annotateVars bounds params n), (annotateVars bounds params v) )
  | annotateVars bounds params (Var(name)) = lookupVar name (params @ bounds)
  | annotateVars bounds params x = x
and lexicalAddressing(x) = annotateVars [] [] x;

val semanticAnalysis =
    lexicalAddressing o
    annotateTC o
    boxSet o
(*    checkDefineContexts o *)
    removeNestedDefine;

end; (* of struct SemanticAnalysis *)
