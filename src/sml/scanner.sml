(* Scanner *)

signature SCANNER =
sig
    val stringToTokens : string -> SchemeToken list;
end;

(* *********************************************************** Scanner *)

exception ErrorNothingAfterHash;
exception ErrorBadChar of char * string;
exception ErrorStringDoesntEnd of string;
exception ErrorNoMetaChar of string;
exception ErrorNoSuchMetaChar of string;
exception ErrorNoChar;
exception ErrorUnknownNamedChar of string;
exception ErrorHash of string;

structure Scanner : SCANNER = 
struct

val whiteChar = makeIsChar(" \t\r\n");
val delimiterChar = makeIsChar("'()\";, \t\r\n");
val octalChar = makeCharInRange(#"0", #"7");
val upperChar = makeCharInRange(#"A", #"Z");
val lowerChar = makeCharInRange(#"a", #"z");
val digitChar = makeCharInRange(#"0", #"9");
val specialSymbolChar = makeIsChar("!@$%^*-_=+<>/?.#");

fun symbolChar (ch) = 
    lowerChar(ch) orelse 
    upperChar(ch) orelse
    digitChar(ch) orelse
    specialSymbolChar(ch);

local
    fun stInit([]) = []
      | stInit(#";" :: s) = stComment(s)
      | stInit(#"(" :: s) = LparenToken :: stInit(s)
      | stInit(#")" :: s) = RparenToken :: stInit(s)
      | stInit(#"'" :: s) = QuoteToken :: stInit(s)
      | stInit(#"#" :: s) = stHash(s)
      | stInit(#"." :: s) = DotToken :: stInit(s)
      | stInit(#"-" :: s) = stMinus(s)
      | stInit(#"\"" ::s) = stString([], s)
      | stInit(ch :: s) = 
            if whiteChar(ch) then stInit(s)
            else if digitChar(ch) then stDecDigit([ch],s)
            else if symbolChar(ch) then stSymbol([ch],s)
            else raise ErrorBadChar(ch, implode(s))
    
    and stMinus([]) = SymbolToken("-") :: stInit([])
      | stMinus(ch :: s) = 
            (* [minus,digit,...] => number *)
            if digitChar(ch) then
                stDecDigit([#"-",ch],s)
            
            (* [minus,non-digit] => symbol *)
            else
                stSymbol([#"-"],[ch] @ s)
    
    and stDecDigit(num,[]) = IntToken(stringToInt num) :: stInit([])
      | stDecDigit(num,ch :: s) =
            if (delimiterChar(ch)) then (* end of int *)
                IntToken(stringToInt num) :: stInit(ch :: s)
            else
                stDecDigit(num @ [ch], s)
    
    and stSymbol(symname,[]) = SymbolToken(implode symname) :: stInit([])
      | stSymbol(symname,ch :: s) =
              if symbolChar(ch) then
                  stSymbol(symname @ [ch],s)
              else
                  SymbolToken(implode symname) :: stInit(ch :: s)
    
    and stString(str, []) = raise ErrorStringDoesntEnd(implode str)
      | stString(str, #"\"" :: s) = StringToken(implode str) :: stInit(s)
      | stString(str, #"\\" :: s) = stStringMetaChar(str,s)
      | stString(str, ch :: s) = stString(str @ [ch], s)
    
    and stStringMetaChar(str,[]) = raise ErrorNoMetaChar(implode str)
      | stStringMetaChar(str, #"t" :: s) = stString(str @ [#"\t"],s)
      | stStringMetaChar(str, #"r" :: s) = stString(str @ [#"\r"],s)
      | stStringMetaChar(str, #"n" :: s) = stString(str @ [#"\n"],s)
      | stStringMetaChar(str, #"\\" :: s) = stString(str @ [#"\\"],s)
      | stStringMetaChar(str, #"\"" :: s) = stString(str @ [#"\""],s)
      | stStringMetaChar(str, ch1 :: ch2 :: ch3 :: s) =
              if andmap octalChar [ch1,ch2,ch3] then
                  stString(str @ [chr( digitToInt(ch1)*8*8
                                      +digitToInt(ch2)*8
                                      +digitToInt(ch3))
                                      ],s)
              else
                raise (ErrorNoSuchMetaChar(implode([ch1,ch2,ch3]@s)))
      | stStringMetaChar(str, s) = raise (ErrorNoSuchMetaChar(implode s))
    
    and stHash([]) = raise ErrorNothingAfterHash
      | stHash(#"t" :: s) = BoolToken(true) :: stInit(s)
      | stHash(#"T" :: s) = BoolToken(true) :: stInit(s)
      | stHash(#"f" :: s) = BoolToken(false) :: stInit(s)
      | stHash(#"F" :: s) = BoolToken(false) :: stInit(s)
      | stHash(#"\\" :: s) = stChar(s)
      | stHash(#"(" :: s) = VectorToken :: stInit(s)
      | stHash(s) = raise ErrorHash("#\\" ^ implode(s))
      
    and stChar([]) = raise ErrorNoChar
      | stChar(ch :: s) = stChar'(s, [ch])
      
    and stChar'([], chars) = makeCharToken(chars) :: stInit([])
      | stChar'(s as ch :: s', chars) = 
        if delimiterChar(ch) then makeCharToken(chars) :: stInit(s)
        else stChar'(s', ch :: chars)
        
    and stComment([]) = stInit([])
      | stComment(#"\n" :: s) = stInit(s)
      | stComment(ch :: s) = stComment(s)
      
    and charsToString(s) = implode(rev(s))
    
    and makeCharToken([ch]) = CharToken(ch)
      | makeCharToken(chars as [ch1, ch2, ch3]) = 
            if (andmap octalChar chars) then
                CharToken(chr(digitToInt(ch1) + 
                    8 * (digitToInt(ch2) + 
                        8 * digitToInt(ch3))))
            else charNameToCharToken(charsToString(chars))
      | makeCharToken(chars) = charNameToCharToken(charsToString(chars))
    
    and charNameToCharToken(charName) = 
    if stringEqual(charName, "space") then CharToken(#" ")
    else if stringEqual(charName, "return") then CharToken(#"\r")
    else if stringEqual(charName, "newline") then CharToken(#"\n")
    else if stringEqual(charName, "tab") then CharToken(#"\t")
    else raise ErrorUnknownNamedChar(charName)
    
    and digitToInt(ch) = ord(ch) - ord(#"0")
    
    and stringToInt(str) = valOf( Int.fromString(implode str) )
in
fun stringToTokens(string) = stInit(explode(string))
end

end;
