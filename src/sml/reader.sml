(* Reader *)

signature READER =
sig
    val stringToSexpr : string -> Sexpr;
    val stringToSexprs : string -> Sexpr list;
end;

(* *********************************************************** Reader *)

exception ErrorNoSexprAfterQuote;
exception ErrorNoClosingRparen;
exception ErrorMissingSexprAfterDot;
exception ErrorInvalidPairSyntax;
exception ErrorCannotReadAllSexprs of (Sexpr list) * (SchemeToken list);
exception ErrorMoreThanOneSexpr;

structure Reader : READER = 
struct
fun getSexpr([], retSexprAndTokens, retNone) = retNone()

  | getSexpr(IntToken(value) :: tokens, retSexprAndTokens, retNone) = 
    retSexprAndTokens(Number(value), tokens)

  | getSexpr(BoolToken(value) :: tokens, retSexprAndTokens, retNone) = 
    retSexprAndTokens(Bool(value), tokens)

  | getSexpr(CharToken(value) :: tokens, retSexprAndTokens, retNone) =
    retSexprAndTokens(Char(value), tokens)

  | getSexpr(StringToken(value) :: tokens, retSexprAndTokens, retNone) = 
    retSexprAndTokens(String(value), tokens)

  | getSexpr(SymbolToken(value) :: tokens, retSexprAndTokens, retNone) = 
    retSexprAndTokens(Symbol(value), tokens)

  | getSexpr(QuoteToken :: tokens, retSexprAndTokens, retNone) =
    (* tokens; ' rest-toks *)
    getSexpr(tokens,
        (fn (quotedSE,restToks) =>
            retSexprAndTokens(Pair(Symbol "quote",Pair(quotedSE,Nil)),restToks)
        ),
        (fn () =>
            (* tokens: ' .
                   or: ' )
              or just: ' *)
            raise ErrorNoSexprAfterQuote))

  | getSexpr(VectorToken :: tokens, retSexprAndTokens, retNone) =
    (* tokens: #( rest-toks *)
    getSexprs(tokens,
        (fn (sExprs,RparenToken :: restToks) =>
            (* tokens: #( se_1 ... se_k ) rest-toks *)
            retSexprAndTokens (Vector(sExprs), restToks)
            
          | (_,DotToken :: restToks) =>
            (* tokens: #( zero-or-more-sexpr . rest-toks *)
            raise ErrorInvalidPairSyntax
            
          | (_,_) =>
            raise ErrorNoClosingRparen))

  | getSexpr(LparenToken :: tokens, retSexprAndTokens, retNone) =
    (* tokens were: ( rest-toks *)
    getSexprs(tokens,
        (fn (sExprs,RparenToken :: restToks) =>
            (* tokens were: ( se_1 se_2 ... se_k ) rest-toks *)
            retSexprAndTokens (MLListToScheme(sExprs), restToks)
            
          | ([],DotToken :: restToks) =>
            (* tokens were: ( . rest-toks *)
            raise ErrorInvalidPairSyntax
            
          | (sExprs,DotToken :: restToks) =>
            (* tokens were: ( se_1 se_2 ... se_k . rest-toks
               then rest-toks should contain EXACTLY ONE sexpr *)
            getSexpr(restToks,
                (fn (lastSE,RparenToken :: restToks) =>
                    (* tokens were: ( se_1 ... se_k . se ) rest-toks *)
                    retSexprAndTokens(
                        combine(MLListToScheme(sExprs),lastSE), restToks)
                    
                  | (lastSE,[]) =>
                    (* tokens were: ( se_1 ... se_k . se
                       then fail: no matching RparenToken *)
                    raise ErrorNoClosingRparen
                    
                  | (lastSE,_) =>
                    (* tokens were: ( se_1 ... se_k . se rest-toks
                       then fail: MORE THAN ONE sexpr after dot *)
                    raise ErrorInvalidPairSyntax),
                (fn () =>
                    (* ZERO expressions after DotToken *)
                    raise ErrorMissingSexprAfterDot))
                    
          | (sExprs, _) =>
            (* tokens were: ( sexpr-list
               then fail: no matching RparenToken *)
            raise ErrorNoClosingRparen))

  | getSexpr(RparenToken :: tokens, retSexprAndTokens, retNone) = retNone()
  | getSexpr(DotToken :: tokens, retSexprAndTokens, retNone) = retNone()

and getSexprs(tokens, retSexprsAndTokens) =
    getSexpr(tokens,
        (fn (se,[]) =>
            (* tokens were: se
               EXACTLY ONE sexpr *)
            retSexprsAndTokens([se],[])
            
          | (se,restToks) =>
            (* tokens were: se rest-toks
               there is more to read *)
            getSexprs(restToks,
                (fn (sExprs,restToks) =>
                    retSexprsAndTokens(se :: sExprs,restToks)))),
        (fn () =>
            (* there was no s-expr in tokens *)
            retSexprsAndTokens([],tokens)))

and tokensToSexprs(tokens) =
    getSexprs(tokens,
                (fn (es, []) =>
                    (* no remaining tokens - all fine *)
                    es

                  | (es, tokens) =>
                    (* some tokens remained *)
                    raise ErrorCannotReadAllSexprs(es, tokens)))

and stringToSexprs(string) =
    tokensToSexprs(Scanner.stringToTokens string)
and stringToSexpr(string) =
    case (stringToSexprs string) of
        [sexpr] => sexpr
      | _ => raise ErrorMoreThanOneSexpr;
end; (* of structure Reader *)
