COMPILE=@gcc -g -c -Wall -ansi
LINK=@gcc

./build:
	mkdir ./build

./build/scheme.o: ./src/c/scheme.c ./build
	@echo Compiling ./build/scheme.o
	$(COMPILE) -o ./build/scheme.o ./src/c/scheme.c

./build/arch.o: ./src/c/arch.c ./build
	@echo Compiling ./build/arch.o
	$(COMPILE) -o ./build/arch.o ./src/c/arch.c

./build/main.o: ./src/c/main.c ./build
	@echo Compiling ./build/main.o
	$(COMPILE) -o ./build/main.o ./src/c/main.c

#./build/builtins.o: ./src/c/builtins.c ./build
#	@echo Compiling ./build/builtins.o
#	$(COMPILE) -o ./build/builtins.o ./src/c/builtins.c

./build/rtemgr.o: ./src/c/rtemgr.c ./build
	@echo Compiling ./build/rtemgr.o
	$(COMPILE) -o ./build/rtemgr.o ./src/c/rtemgr.c

%: %.c ./build ./build/scheme.o ./build/arch.o ./build/main.o ./build/rtemgr.o
	@echo Compiling $*.o
	$(COMPILE) -I ./src/c/ -I ./inc/ -o $*.o $*.c
	@echo Linking: $*
	$(LINK) -o $* $*.o ./build/main.o ./build/scheme.o ./build/arch.o ./build/rtemgr.o
